package explr.utils;

import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.text.ParsePosition;

public class Utils {
    /**
    *     @return null on parse error
    */
    public static Integer parseInt(String source){
        NumberFormat formatter = NumberFormat.getIntegerInstance();
        formatter.setParseIntegerOnly(true);
        Number parsed = formatter.parse(source, new ParsePosition(0));
        if(parsed==null){
            return null;
        }
        return parsed.intValue();
    }

    public static String methodParamsToString(Method mtd){
        StringBuilder sb = new StringBuilder();
        if(mtd!=null) {
            sb.append("(");
            Class[] params = mtd.getParameterTypes();
            for (int i = 0; i < params.length; i++) {
                sb.append(params[i].getSimpleName());
                if (i != params.length - 1) {
                    sb.append(", ");
                }
            }
            sb.append(")");
        }else{
            sb.append("NULL_METHOD");
        }
        return sb.toString();
    }
}
