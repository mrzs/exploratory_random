package explr.tests;

import explr.core.TestCaseGenerator;
import org.testng.annotations.Test;

public class TestCaseGeneratorTest {
    @Test
    public void testReflection(){
        TestCaseGenerator generator = new TestCaseGenerator();
        for(int i = 0; i < 1000; i++)
        generator.generateTestCase(10,TestCaseGenerator.USE_PAGE_SANITY_VERIFICATIONS).execute();
    }
}