package explr.tests;

import explr.pages.TenMinutesMailPage;
import explr.pages.protonmail.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class SmokeTests extends AbstractTestCase {
    //    private String myMailAddress = "autotest@protonmail.ch";
    private String login = "autotest";
    private String password = "mySuperPa33";
    private String mailboxPassword = "mymailboxPa33";
    private String fakeEmail = "me@myself.com";
    private String subject = "Seen that?";
    private String message = "Look at my mail! My mail is amazing!";

static{
    System.setProperty("webdriver.chrome.driver","./webdrivers/chromedriver.exe");
}

    @Test(dataProvider = "WebDriverProvider")
    public void testDraftCreateAndDelete(WebDriver driver) {
        InboxMailPage inbox = login(driver);
        ComposeMailPage composePage = inbox.compose()
                .setTo(fakeEmail)
                .setSubject(subject)
                .setMessage(message);
        composePage.saveDraft()
                .verifyMessagePresent(subject)
                .deleteDrafts(subject)
                .verifyMessageNotPresent(subject);
    }

    @Test(dataProvider = "WebDriverProvider")
    public void testMailEncryptionAndDecryption(WebDriver driver){
        String mailPassword = "myMessagePass";
        String mailPassHint = "Hey!<p> I am a hint!";
        TenMinutesMailPage tempMailPage = new TenMinutesMailPage(driver,false);
        String tempMail = tempMailPage.navigate().getCurrentAdress();
        InboxMailPage inbox = login(driver);
        ComposeMailPage composePage = inbox.compose()
                .setTo(tempMail)
                .setSubject(subject)
                .setMessage(message)
                .setEncryption(mailPassword,mailPassHint);
        composePage.send();
        DecryptMailPage decryptPage = new TenMinutesMailPage(driver)
                .navigate()
                .waitForAnyMail(60)
                .goToReceivedDecryptLink();
        DecryptedMessageMailPage decrypted = decryptPage.decrypt(mailPassword);
        decrypted
                .verifySubject(subject)
                .verifyMessage(message);
    }

    @Test(dataProvider = "WebDriverProvider")
    public void testContacts(WebDriver driver){
        String name = "Rob Johnson";
        String email = "robin@john.com";
        String editedName = "Rob Junior Johnson";
        String editedEmail = "robin-juniour@john.com";
        InboxMailPage inbox = login(driver);
        ContactsPage contactsPage = inbox.contacts();
        contactsPage
                .createContact(name,email)
                .verifyContactExists(name, email)
                .editContact(name, email, editedName, editedEmail)
                .verifyContactExists(editedName, editedEmail)
                .deleteContact(editedName, editedEmail)
                .verifyContactNotExists(editedName, editedEmail);
    }

    @Test(dataProvider = "WebDriverProvider")
    public void testTrashAndRestore(WebDriver driver){
        InboxMailPage inbox = login(driver);
        ComposeMailPage composePage = inbox.compose()
                .setTo(fakeEmail)
                .setSubject(subject)
                .setMessage(message);
        DraftsMailPage drafts = composePage.saveDraft()
                .verifyMailExists(fakeEmail,subject)
                .deleteDrafts(subject)
                .verifyMailNotExists(fakeEmail,subject);
        TrashMailPage trashMailPage = drafts.trash();
        InboxMailPage inboxMailPage = trashMailPage.verifyMailExists(login,subject)
                .selectBySubject(subject)
                .moveToInbox()
                .inbox();
        inboxMailPage.verifyMailExists(login,subject)
                .selectBySubject(subject)
                .deleteMails(subject)
                .verifyMailNotExists(login,subject);
        trashMailPage = inboxMailPage.trash();
        trashMailPage.verifyMailExists(login,subject)
                .deleteAllTrash()
                .verifyMailNotExists(login,subject);
    }

    @Test(dataProvider = "WebDriverProvider")
    public void testStarredMessages(WebDriver driver){
        String starSubject = subject+" *StarreD*";
        InboxMailPage inbox = login(driver);
        ComposeMailPage composePage = inbox.compose()
                .setTo(fakeEmail)
                .setSubject(starSubject)
                .setMessage(message);
        DraftsMailPage draftsPage = composePage.saveDraft()
                .verifyMessageNotStarred(fakeEmail,starSubject)
                .setMessagesStarred(true,fakeEmail,starSubject)
                .verifyMessageStarred(fakeEmail,starSubject);
        StarredMailPage starredPage = draftsPage.starred()
                .verifyMailExists(login, starSubject)
                .setMessagesStarred(false, login, starSubject)
                .verifyMailNotExists(login, starSubject);
        starredPage.drafts()
                .deleteDrafts(starSubject);
    }
    private InboxMailPage login(WebDriver driver){
        return new MainLoginPage(driver)
                .login(login, password)
                .unlockMailbox(mailboxPassword);
    }
}
