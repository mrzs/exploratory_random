package explr.tests;

import explr.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public abstract class AbstractTestCase {
    private static String currentlyLaunchedBrowser;
    private static Object[][] cachedBrowserDrivers;
    protected boolean restartBrowserForEachTestCase = true;

    @Parameters("browser")
    @BeforeTest
    public void checkBrowser(String browser) {
        if (currentlyLaunchedBrowser == null || !currentlyLaunchedBrowser.equals(browser)) {
            setBrowserDrivers(browser);
            currentlyLaunchedBrowser = browser;
        }
    }

    @AfterMethod
    @Parameters("browser")
    public void restartBrowser(String browser) {
        if(this.restartBrowserForEachTestCase){
            quitAllBrowsers();
            setBrowserDrivers(browser);
        }
    }

    @DataProvider(name = "WebDriverProvider")
    public Object[][] provide(Method m) {
        return cachedBrowserDrivers;
    }

    @AfterSuite
    public void tearDown() {
        quitAllBrowsers();
    }

    private void setBrowserDrivers(String browser) {
        quitAllBrowsers();

        if (browser!=null && browser.equalsIgnoreCase("ie")) {
            InternetExplorerDriver driver = new InternetExplorerDriver(getLoggingCapabilities());
            cachedBrowserDrivers = new Object[][]{
                    new Object[]{driver}
            };
        } else if (browser!=null && browser.equalsIgnoreCase("firefox")) {
            FirefoxDriver driver = new FirefoxDriver(getLoggingCapabilities());
            cachedBrowserDrivers = new Object[][]{
                    new Object[]{driver}
            };
        } else if (browser!=null && browser.equalsIgnoreCase("chrome")) {
            ChromeDriver driver = new ChromeDriver(getLoggingCapabilities());
            cachedBrowserDrivers = new Object[][]{
                    new Object[]{driver}
            };
        } else {
            throw new RuntimeException("Parameter \"browser\" is not set or contains unknown value. \n " +
                    "Supported values = [ie, firefox, chrome]" +
                    "Current value: " + browser);
        }
        //Setting timeouts
        setTimeOuts();
    }


    private void setTimeOuts() {
        String implicitTimeOutString = System.getProperty("webtest.default.implicitlyWaitSeconds", "0");
        Integer implicitTimeOut = Utils.parseInt(implicitTimeOutString);
        if (implicitTimeOut != null) {
            for (Object[] driverArr : cachedBrowserDrivers) {
                for (Object driver : driverArr) {
                    ((WebDriver) driver).manage().timeouts().implicitlyWait(implicitTimeOut, TimeUnit.SECONDS);
                }
            }
        }
    }

    private void quitAllBrowsers() {
        if (cachedBrowserDrivers != null) {
            for (Object[] driverArr : cachedBrowserDrivers) {
                for (Object driver : driverArr) {
                    ((WebDriver) driver).quit();
                }
            }
        }
    }

//    private FirefoxDriver getFirefoxDriver() {
//        FirefoxProfile ffProfile = new FirefoxProfile();
//        try {
//            JavaScriptError.addExtension(ffProfile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return new FirefoxDriver(ffProfile);
//    }

    private DesiredCapabilities getLoggingCapabilities(){
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        LoggingPreferences prefs = new LoggingPreferences();
        prefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, prefs);
        return capabilities;
    }
}
