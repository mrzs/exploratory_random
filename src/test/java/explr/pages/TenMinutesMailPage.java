package explr.pages;

import explr.core.annotations.Default;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.pages.protonmail.DecryptMailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@IgnoreInRandomTesting
public class TenMinutesMailPage extends AbstractPage{
    static private final String URL = "http://10minutemail.com/";

    private String currentAddress;
    public static final String TITLE ="10 Minute Mail";
    private static String currentAddressCss = "form#addyForm > input[readonly]";
    private static String tenMoreMinutesCss = "div#leftpart > a";
    private static String mailListCss = "table#emailTable > tbody";
    public TenMinutesMailPage(WebDriver driver){
        this(driver, false);
    }
    public TenMinutesMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    public String getPageUrl(){
        return URL;
    }
    public String getTitle(){
        return TITLE;
    }
    public TenMinutesMailPage navigate() {
        getDriver().get(URL);
        waitForElementToAppearByCss(tenMoreMinutesCss,5);
        tenMoreMinutes();
        setCurrentAddress();
        return this;
    }
    public String getCurrentAdress(){
        return currentAddress;
    }
    private void setCurrentAddress(){
        currentAddress = getByCss(currentAddressCss).getAttribute("value");
    }
    public TenMinutesMailPage tenMoreMinutes(){
        getByCss(tenMoreMinutesCss).click();
        return this;
    }

    //FYI: This method is crap; Crap site, crap code.
    public DecryptMailPage goToReceivedDecryptLink(/*String subject*/){
        getByCss(mailListCss).findElement(By.cssSelector("TR > TD > A")).click();
        waitForElementToAppearByCss("div.article > a",10);
        WebElement decryptMailLink = getByCss("div.article > a");
        decryptMailLink.click();
        return new DecryptMailPage(getDriver(),false);
    }
    public TenMinutesMailPage waitForAnyMail(@Default(longh = 45) long timeout){
        waitForElementToAppearByCss(mailListCss+" > tr",timeout);
        return this;
    }
    public static TenMinutesMailPage getInstance(WebDriver driver) {
        return new TenMinutesMailPage(driver,true);
    }
}
