package explr.pages;

import explr.pages.protonmail.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public abstract class AbstractPage {
    private WebDriver driver;
    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 15;
    public static final String TITLE = "ProtonMail";
    /*
    private static long pageLoadTimeout;
    static{
        String timeoutStr = System.getProperty("webtest.default.implicitlyWaitSeconds", "0");
        pageLoadTimeout=Utils.parseInt(timeoutStr);
    }
    */
    /**
     * @return null if it is not possible to determine URL at the moment or there is no need in navigating there in the constructor
     */
    public abstract String getPageUrl();

    public abstract String getTitle();

    protected abstract <T extends AbstractPage>T navigate();

    protected AbstractPage(WebDriver driver){
        this(driver, true);
    }

    protected AbstractPage(WebDriver driver, boolean navigate){
        this.driver=driver;
        String currentUrl = driver.getCurrentUrl();
        String thisPageUrl = getPageUrl();
        if(navigate && thisPageUrl!=null && !currentUrl.equalsIgnoreCase(thisPageUrl)){
//            driver.get(thisPageUrl); //navigate to this page if not yet there
            navigate();
        }/*
        else{
            Wait<WebDriver> wait = new WebDriverWait(driver, pageLoadTimeout);
            wait.until(new Function<WebDriver, Boolean>(){
                @Override
                public Boolean apply(WebDriver driver){
                    return "complete".equals(((JavascriptExecutor) driver).executeScript("return document.readyState"));
                }
            });
        }
        */
    }

    protected WebDriver getDriver(){
        return driver;
    }

    protected WebElement getByCss(String cssSelector){
        return driver.findElement(By.cssSelector(cssSelector));
    }
    protected void waitForElementToAppearByCss(String css,long timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
    }
    protected void waitForElementToDisappearByCss(String css, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        By byCss = By.cssSelector(css);
        int elementsFound = driver.findElements(byCss).size();
        if(elementsFound!=0) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(byCss));
        }
    }

    public void sanityCheck() {
        String currentTitle = driver.getTitle();
        String expectedTitle = this.getTitle();
        Assert.assertTrue(currentTitle.matches(expectedTitle),"Current title: '"+currentTitle+"' not matches pattern: '"+expectedTitle+"'");
        String browserError = "Browser encountered such errors: ";

        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        Assert.assertTrue(logEntries.getAll().isEmpty(), browserError + logEntries.getAll().toString()); //Check errors in logs

        String errorMessage = checkIfOnErrorPage();
        Assert.assertNull(errorMessage,"\n"+errorMessage+"\n");
    }

    private String checkIfOnErrorPage(){
        List<WebElement> middleWraps = driver.findElements(By.cssSelector("div.panel.wrap"));
        if(!middleWraps.isEmpty()){
            for (WebElement wrapper : middleWraps) {
                String text = wrapper.getText();
                if(text!=null && (text.contains("Error") || text.contains("error"))){
                    return text;
                }else{
                    return null;
                }
            }
        }
        return null;
    }

/* SPECIAL "jumpToPAge" methods for random generation */
    public DraftsMailPage jumpToDraftsMailPage(){
        return new DraftsMailPage(driver,true);
    }
    public MainLoginPage jumpToMainLoginPage(){
        return new MainLoginPage(driver,true);
    }
    public ContactsPage jumpToContactsPage(){
        return new ContactsPage(driver,true);
    }
    public MailboxLoginPage jumpToMailboxLoginPage(){
        return new MailboxLoginPage(driver,true);
    }
    public InboxMailPage jumpToInboxMailPage(){
        return new InboxMailPage(driver,true);
    }
    public ComposeMailPage jumpToComposeMailPage(){
        return new ComposeMailPage(driver,true);
    }
//    public DecryptMailPage jumpToDecryptMailPage(){ //Ignore this page
//        return new DecryptMailPage(driver,true);
//    }
    public StarredMailPage jumpToStarredMailPage(){
        return new StarredMailPage(driver,true);
    }
    public TrashMailPage jumpToTrashMailPage(){
        return new TrashMailPage(driver,true);
    }
//    public TenMinutesMailPage jumpToTenMinutesMailPage(){  //Ignore this page
//        return new TenMinutesMailPage(driver,true);
//    }
//    public DecryptedMessageMailPage jumpToDecryptedMessageMailPage(){ //Ignore this page
//        return new DecryptedMessageMailPage(driver,true);
//    }
}