package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static explr.core.common.DefaultTestData.*;

public class StarredMailPage extends MailPageWithMenus<StarredMailPage> {
    static private final String URL = "https://protonmail.ch/starred/d";
    //    static private final String TITLE = "ProtonMail";
    public StarredMailPage(WebDriver driver){
        this(driver, true);
    }
    public StarredMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  StarredMailPage getInstance(WebDriver driver) {
        return new StarredMailPage(driver,true);
    }
    public StarredMailPage navigate() {
        getDriver().get(URL);
        waitForLoadingArea(PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }

    @Override
    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"setMessagesStarred","saveDraft","setSubject","setTo"})
    public StarredMailPage deleteMails(@Default(string = SUBJECT) String subject) {
        return super.deleteMails(subject);
    }
    @Override
    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"setMessagesStarred","saveDraft","setSubject","setTo"})
    public StarredMailPage setMessagesStarred(@Default(bool = IS_NOT_STARED) boolean starred, @Default(string = LOGIN) String sender, @Default(string = SUBJECT) String subject) {
        waitForLoadingArea();
        return super.setMessagesStarred(starred, sender,subject);
    }

    @Override
    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"setMessagesStarred","saveDraft","setSubject","setTo"})
    public StarredMailPage selectBySubject(@Default(string = SUBJECT) String subject) {
        String selectBySubjectCss = "table#messages > tbody > tr[data-subject='   %s']";
        String selectChcBoxCss = "input[type=\"checkbox\"]";

        waitForLoadingArea();
        subject = String.format(selectBySubjectCss,subject);
        List<WebElement> mails = getDriver().findElements(By.cssSelector(subject));
        for(WebElement mail : mails){
            String attribute = mail.getAttribute("data-subject");
            WebElement chkBox = mail.findElement(By.cssSelector(selectChcBoxCss));
            if(!chkBox.isSelected()){
                chkBox.click();
            }
        }
        return getThis();
    }

    @IgnoreInRandomTesting
    public StarredMailPage getThis(){
        return this;
    }
}
