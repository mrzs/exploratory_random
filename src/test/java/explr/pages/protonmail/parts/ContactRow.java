package explr.pages.protonmail.parts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ContactRow {
    private WebElement row;
    private String nameCss = "input[name='contact_name']";
    private String emailCss = "input[name='contact_email']";
    private String editBtnCss = "td.text-right > button[name='edit']";
    private String saveBtnCss = "td.text-right > button[name='update']";
    private String deleteBtnCss = "td.text-right > button[name='delete']";
    public ContactRow(WebElement row){
        if(!"tr".equalsIgnoreCase(row.getTagName())){
            throw new IllegalArgumentException("<"+row.getTagName() + "> element received. <tr> element expected.");
        }
        this.row = row;
    }
    public String getName(){
        return getByCss(nameCss).getAttribute("value");
    }
    public String getEmail(){
        return getByCss(emailCss).getAttribute("value");
    }
    public WebElement getRow(){ return row; }
    public ContactRow edit(String newName,String newEmail){
        String clearSeq = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;
        getByCss(editBtnCss).click();
        getByCss(nameCss).sendKeys(clearSeq+newName);
        getByCss(emailCss).sendKeys(clearSeq+newEmail);
        getByCss(saveBtnCss).click();
        return this;
    }
    public void delete(){
        getByCss(deleteBtnCss).click();
    }
    private WebElement getByCss(String css){
        return row.findElement(By.cssSelector(css));
    }
}
