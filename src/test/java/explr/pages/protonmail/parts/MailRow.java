package explr.pages.protonmail.parts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MailRow {
    private WebElement row;
    private String senderCss = "span.sender_span";
    private String subjectCss = "td.table_subject > a.subject_link > strong";
    private String starBtnCss = "span.actions_wrapper > span[class^=\"fa fa-star\"]";
    public MailRow(WebElement row){
        if(!"tr".equalsIgnoreCase(row.getTagName())){
            throw new IllegalArgumentException("<"+row.getTagName() + "> element received. <tr> element expected.");
        }
        this.row = row;
    }
    public WebElement getRow(){ return row; }
    public String getSender(){
        return getByCss(senderCss).getText();
    }
    public String getSubject(){
        return getByCss(subjectCss).getText();
    }
    public boolean isStarred(){
        String starBtnClass = getByCss(starBtnCss).getAttribute("class");
        return "fa fa-star".equals(starBtnClass);
    }
    public void setStared(boolean starred) {
        if(isStarred()!=starred){
            getByCss(starBtnCss).click();
        }
    }
    private WebElement getByCss(String css){
        return row.findElement(By.cssSelector(css));
    }
}
