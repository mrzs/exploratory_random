package explr.pages.protonmail.parts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class MailListHeaderMenu {
    private WebDriver driver;
    private String selectAllChckBoxCss = "input#everything[type=\"checkbox\"]";
    private String deleteBtnCss = "button#mass_trash_messages";
    public MailListHeaderMenu(WebDriver driver){
        this.driver = driver;
    }
    public void checkSelectAll(){
        WebElement chkBox = driver.findElement(By.cssSelector(selectAllChckBoxCss));
        if(!chkBox.isSelected()){
            chkBox.click();
        }
    }
    public void delete(){
        WebElement deleteButton =  driver.findElement(By.cssSelector(deleteBtnCss));
        String isDisabled = deleteButton.getAttribute("disabled");
        String declaredClass = deleteButton.getAttribute("class");
        if("true".equals(isDisabled) || declaredClass.endsWith("disabled")){
            Assert.fail("Delete button is not enabled.");
        }else{
            deleteButton.click();
        }
    }
}