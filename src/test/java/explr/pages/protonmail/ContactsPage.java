package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.common.DefaultTestData;
import explr.pages.protonmail.parts.ContactRow;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class ContactsPage extends AbstractLoggedInPage{
    static private final String URL = "https://protonmail.ch/contacts";
//    static private final String TITLE = "ProtonMail";
    private String addContactBtnCss = "a#addContact.pure-button";
    private String nameInputCss = "input#addContactName.form-control";
    private String emailInputCss = "input#addContactEmail.form-control";
    private String saveContactBtnCss = "button#saveCtoDB";
    private String contactsTableCss = "table#contactsTable > tbody > tr";
    public ContactsPage(WebDriver driver){
        this(driver, true);
    }
    public ContactsPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }

    @Override
    @IgnoreInRandomTesting
    public AbstractLoggedInPage getThis() {
        return this;
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  ContactsPage getInstance(WebDriver driver) {
        return new ContactsPage(driver,true);
    }
    public ContactsPage navigate() {
        getDriver().get(URL);
        waitForLoadingArea(PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }
    public ContactsPage createContact(@Default(string = DefaultTestData.CONTACT_NAME) String name, @Default(string = DefaultTestData.CONTACT_EMAIL) String email){
        waitForLoadingArea();
        getByCss(addContactBtnCss).click();
        getByCss(nameInputCss).sendKeys(name);
        getByCss(emailInputCss).sendKeys(email);
        getByCss(saveContactBtnCss).click();
        return this;
    }
    @Depends(clas = ContactsPage.class, method = "createContact")
    public ContactsPage editContact(@Default(string = DefaultTestData.CONTACT_NAME)  String name, @Default(string = DefaultTestData.CONTACT_EMAIL)String email, @Default(string = DefaultTestData.CONTACT_EDITED_NAME)String newName,@Default(string = DefaultTestData.CONTACT_EDITED_EMAIL)String newEmail){
        ContactRow contact = findContact(name,email);
        contact.edit(newName, newEmail);
        return this;
    }
    public ContactsPage verifyContactExists(@Default(string = DefaultTestData.CONTACT_NAME) String name, @Default(string = DefaultTestData.CONTACT_EMAIL) String email){
        ContactRow contact = findContact(name,email);
        Assert.assertTrue(contact != null);
        return this;
    }
    public ContactsPage verifyContactNotExists(@Default(string = DefaultTestData.CONTACT_NAME) String name, @Default(string = DefaultTestData.CONTACT_EMAIL)String email){
        ContactRow contact = findContact(name,email);
        Assert.assertTrue(contact == null);
        return this;
    }
    @Depends(clas = {ContactsPage.class}, method = {"createContact"})
    public ContactsPage deleteContact(@Default(string = DefaultTestData.CONTACT_NAME)String name,@Default(string = DefaultTestData.CONTACT_EMAIL) String email){
        ContactRow contact = findContact(name,email);
        contact.delete();
        return this;
    }
    private ContactRow findContact(@Default(string = DefaultTestData.CONTACT_NAME) String name, @Default(string = DefaultTestData.CONTACT_EMAIL) String email){
        List<WebElement> rows = getDriver().findElements(By.cssSelector(contactsTableCss));
        for(WebElement rowElement : rows){
            ContactRow row = new ContactRow(rowElement);
            if(name.equals(row.getName()) && email.equals(row.getEmail())){
                return row;
            }
        }
        return null;
    }
}
