package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.annotations.OnPageChange;
import explr.core.common.DefaultTestData;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

@OnPageChange(clas = ComposeMailPage.class, method = "acceptAlerts")
public class ComposeMailPage extends AbstractLoggedInPage {
    static private final String URL = "https://protonmail.ch/compose";
    static private final int WAIT_FOR_ALERT = 3;
    private String toInputCss = "input#RecipientList";
    private String subjectInputCss = "input#MessageTitle";
    private String saveBtnCss = "button#saveDraftBtn";
    private String encryptionChkBoxCss = "input#encOutside";
    private String encryptPassInputCss = "input#outsidePw";
    private String encryptPassConfirmInputCss = "input#outsidePwConfirm";
    private String encryptHintInputCss = "input#outsidePwHint";
    private String sendBtnCss = "button#compose_send";
    private String discardBtnCss = "button#deleteDraft";
    public ComposeMailPage(WebDriver driver){
        this(driver, true);
    }
    public ComposeMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  ComposeMailPage getInstance(WebDriver driver) {
        return new ComposeMailPage(driver,true);
    }
    @IgnoreInRandomTesting
    public ComposeMailPage navigate() {
        getDriver().get(URL);
        waitForLoadingArea(PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }
    public ComposeMailPage setTo(@Default(string = DefaultTestData.SENDER) String to){
        getByCss(toInputCss).sendKeys(to);
        return this;
    }
    public ComposeMailPage setSubject(@Default(string = DefaultTestData.SUBJECT)  String subject){
        getByCss(subjectInputCss).sendKeys(subject);
        return this;
    }
    public ComposeMailPage setMessage(@Default(string = DefaultTestData.MESSAGE) String message){
        ((JavascriptExecutor) getDriver())
                .executeScript("document.getElementById('editor').contentDocument.body.innerHTML = \'"+message+"\';");
        return this;
    }
    public ComposeMailPage setEncryption(@Default(string = DefaultTestData.MAIL_PASSWORD) String password,@Default(string = DefaultTestData.MAIL_PASS_HINT) String hint){
        WebElement encrptChkBox = getByCss(encryptionChkBoxCss);
        if(!encrptChkBox.isSelected()){
            encrptChkBox.click();
        }
        String clearSeq = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;
        getByCss(encryptPassInputCss).sendKeys(clearSeq + password);
        getByCss(encryptPassConfirmInputCss).sendKeys(clearSeq + password);
        getByCss(encryptHintInputCss).sendKeys(clearSeq + hint);
        return this;
    }
    @Depends(clas = ComposeMailPage.class, method = "setTo")
    public InboxMailPage send(){
        getByCss(sendBtnCss).click();
        waitForElementToAppearByCss("div.growl",5);
        return new InboxMailPage(getDriver(),true);
    }
    public DraftsMailPage saveDraft(){
        getByCss(saveBtnCss).click();
        waitForElementToAppearByCss("div.growl",5);
        return new DraftsMailPage(getDriver(),true);
    }
    @Override
    @IgnoreInRandomTesting
    public ComposeMailPage getThis(){
        return this;
    }

    @IgnoreInRandomTesting
    public void acceptAlerts() {
        WebDriver driver = getDriver();
        if(getAlert()==null) {
            List<WebElement> discardBtnList = driver.findElements(By.cssSelector(discardBtnCss));
            if(discardBtnList.size() != 0) discardBtnList.get(0).click();
        }
        WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_FOR_ALERT);
        try {
            webDriverWait.until(ExpectedConditions.alertIsPresent());
        }catch (TimeoutException e){
            // Ignore if no alertIsPresent
        }
        int maxAlerts = 10;
        int i = 0;
        Alert alert = getAlert();
        while(alert != null && i < maxAlerts){
            alert.accept();
            alert = getAlert();
            i++;
        }
    }

    private Alert getAlert(){
        WebDriver driver = getDriver();
        try {
            return  driver.switchTo().alert();
        } catch (NoAlertPresentException e) {
            return null;
        }
    }

    @Override
    public InboxMailPage inbox() {
        waitForLoadingArea();
        getByCss(inboxBtnCss).click();
        acceptAlerts();
        return new InboxMailPage(getDriver(),true);
    }
    @Override
    public DraftsMailPage drafts() {
        waitForLoadingArea();
        getByCss(draftsBtnCss).click();
        acceptAlerts();
        return new DraftsMailPage(getDriver(),true);
    }
    @Override
    public ComposeMailPage compose() {
        waitForLoadingArea();
        getByCss(composeBtnCss).click();
        acceptAlerts();
        return new ComposeMailPage(getDriver(),false);

    }
    @Override
    public ContactsPage contacts() {
        waitForLoadingArea();
        getByCss(contactsBtnCss).click();
        acceptAlerts();
        return new ContactsPage(getDriver(),true);
    }
    @Override
    public TrashMailPage trash() {
        waitForLoadingArea();
        getByCss(trashBtnCss).click();
        acceptAlerts();
        return new TrashMailPage(getDriver(),true);
    }
    @Override
    public StarredMailPage starred() {
        waitForLoadingArea();
        getByCss(starredBtnCss).click();
        acceptAlerts();
        return new StarredMailPage(getDriver(),true);
    }

    @Override
    public DraftsMailPage jumpToDraftsMailPage() {
        acceptAlerts();
        return super.jumpToDraftsMailPage();
    }
    @Override
    public MainLoginPage jumpToMainLoginPage() {
        acceptAlerts();
        return super.jumpToMainLoginPage();
    }
    @Override
    public ContactsPage jumpToContactsPage() {
        acceptAlerts();
        return super.jumpToContactsPage();
    }
    @Override
    public MailboxLoginPage jumpToMailboxLoginPage() {
        acceptAlerts();
        return super.jumpToMailboxLoginPage();
    }
    @Override
    public InboxMailPage jumpToInboxMailPage() {
        acceptAlerts();
        return super.jumpToInboxMailPage();
    }
    @Override
    public ComposeMailPage jumpToComposeMailPage() {
        acceptAlerts();
        return super.jumpToComposeMailPage();
    }
    @Override
    public StarredMailPage jumpToStarredMailPage() {
        acceptAlerts();
        return super.jumpToStarredMailPage();
    }
    @Override
    public TrashMailPage jumpToTrashMailPage() {
        acceptAlerts();
        return super.jumpToTrashMailPage();
    }
}
