package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.pages.protonmail.parts.MailListHeaderMenu;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static explr.core.common.DefaultTestData.*;
public class TrashMailPage extends MailPageWithMenus<TrashMailPage>{
    static private final String URL = "https://protonmail.ch/trash";

    private String moveToInboxBtnCss = "button#mass_move_to_inbox";
    private String deleteAllTrashBtnCss = "button#delete_all_trash";
    public TrashMailPage(WebDriver driver){
        this(driver, true);
    }
    public TrashMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
        headerMenu = new MailListHeaderMenu(driver);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  TrashMailPage getInstance(WebDriver driver) {
        return new TrashMailPage(driver,true);
    }
    public TrashMailPage navigate(){
        getDriver().get(URL);
        waitForLoadingArea(PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }

    @Override
    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"deleteDrafts","saveDraft","setSubject","setTo"})
    protected TrashMailPage setMessagesStarred(@Default(bool = IS_STARED) boolean starred, @Default(string = SENDER) String sender, @Default(string = SUBJECT) String subject) {
        return super.setMessagesStarred(starred, sender, subject);
    }
    @Override
    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"deleteDrafts","saveDraft","setSubject","setTo"})
    protected TrashMailPage deleteMails(@Default(string = SUBJECT) String subject) {
        return super.deleteMails(subject);
    }

    @IgnoreInRandomTesting
    public TrashMailPage getThis(){
        return this;
    }

    @Depends(clas = {TrashMailPage.class, DraftsMailPage.class}, method = {"selectBySubject", "deleteMails"})
    public TrashMailPage moveToInbox(){
        getByCss(moveToInboxBtnCss).click();
        return this;
    }

    @Depends(clas = {DraftsMailPage.class,ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"deleteDrafts","saveDraft","setSubject","setTo"})
    public TrashMailPage deleteAllTrash(){
        getByCss(deleteAllTrashBtnCss).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 3);
        wait.until(ExpectedConditions.alertIsPresent());
        getDriver().switchTo().alert().accept();
        waitForElementToAppearByCss("div#nothin",5);
        return this;
    }
}
