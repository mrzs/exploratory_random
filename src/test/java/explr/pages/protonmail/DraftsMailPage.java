package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.common.DefaultTestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

import static explr.core.common.DefaultTestData.*;
public class DraftsMailPage extends MailPageWithMenus<DraftsMailPage> {
    static private final String URL = "https://protonmail.ch/draft";
//    static private final String TITLE = "ProtonMail";
//    private String selectBySubjectCss = "table#messages > tbody > tr[data-subject=\"%s\"]";
    private String emptyListMessageCss = "div#listing > div#nothin";
//    private String selectChcBoxCss = "input[type=\"checkbox\"]";
    public DraftsMailPage(WebDriver driver){
        this(driver, true);
    }
    public DraftsMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  DraftsMailPage getInstance(WebDriver driver) {
        return new DraftsMailPage(driver,true);
    }
    public DraftsMailPage navigate() {
        getDriver().get(URL);
        waitForElementToAppearByCss("body#page-draft", PAGE_LOAD_TIMEOUT_SECONDS);
        waitForLoadingArea(PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }
    @IgnoreInRandomTesting
    public DraftsMailPage getThis(){
        return this;
    }
    @Depends(clas = {ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"saveDraft","setSubject","setTo"})
    public DraftsMailPage deleteAll(){
        headerMenu.checkSelectAll();
        headerMenu.delete();
        return this;
    }
    @Depends(clas = {ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"saveDraft","setSubject","setTo"})
    public DraftsMailPage deleteDrafts(@Default(string = DefaultTestData.SUBJECT) String subject){
        deleteMails(subject);
        return this;
    }
//    @Override
//    public DraftsMailPage selectBySubject(String subject){
//        subject = String.format(selectBySubjectCss,subject);
//        List<WebElement> drafts = getDriver().findElements(By.cssSelector(subject));
//        for(WebElement draft : drafts){
//            WebElement chkBox = draft.findElement(By.cssSelector(selectChcBoxCss));
//            if(!chkBox.isSelected()){
//                chkBox.click();
//            }
//        }
//        return this;
//    }
    public DraftsMailPage verifyMessagePresent(@Default(string = DefaultTestData.SUBJECT) String subject){
        List<WebElement> messageList = findBySubject(subject);
        Assert.assertTrue(messageList.size()>0, "\nValidation Failed: "
                +" Message with subject:'"+subject+"' "
                +"not found in drafts folder.");
//        if(messageList.size()>1) log.warning("Found More than one msg");
        return this;
    }
    public DraftsMailPage verifyMessageNotPresent(@Default(string = DefaultTestData.SUBJECT) String subject){
        List<WebElement> messageList = findBySubject(subject);
        Assert.assertTrue(messageList.size()==0, "\nValidation Failed: "
                +" Message with subject:'"+subject+"' "
                +"was found in drafts folder.");
        return this;
    }
    public DraftsMailPage verifyIsEmpty(){
        int messagesFound = getDriver().findElements(By.cssSelector(emptyListMessageCss)).size();
        Assert.assertTrue(messagesFound==1,"Message list is not empty");
        return this;
    }
    @Override
    @Depends(clas = {ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"saveDraft","setSubject","setTo"})
    public DraftsMailPage setMessagesStarred(@Default(bool = IS_STARED) boolean starred, @Default(string = SENDER) String sender, @Default(string = SUBJECT) String subject) {
        return super.setMessagesStarred(starred, sender, subject);
    }
    @Override
    @Depends(clas = {ComposeMailPage.class,ComposeMailPage.class,ComposeMailPage.class}, method = {"saveDraft","setSubject","setTo"})
    public DraftsMailPage deleteMails(@Default(string = SUBJECT) String subject) {
        return super.deleteMails(subject);
    }
}
