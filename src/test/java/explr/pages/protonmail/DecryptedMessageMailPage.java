package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.common.DefaultTestData;
import explr.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
@IgnoreInRandomTesting
public class DecryptedMessageMailPage extends AbstractPage {
    static private final String URL = "https://protonmail.ch/decrypt/"; //useless
//    static private final String TITLE = "ProtonMail";
    private static String subjectTextCss = "div.middlepanel.container > div > h1#pmTitle";
    private static String messageTextCss = "div#protonMailMessage";
    public DecryptedMessageMailPage(WebDriver driver){
        this(driver, false);
    }
    public DecryptedMessageMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    public String getPageUrl(){
        return URL;
    }
    public String getTitle(){
        return TITLE;
    }
    public static  DecryptedMessageMailPage getInstance(WebDriver driver) {
        return new DecryptedMessageMailPage(driver,true);
    }
    public DecryptedMessageMailPage navigate() {
        getDriver().get(URL);
        waitForElementToDisappearByCss("div#loadingArea", PAGE_LOAD_TIMEOUT_SECONDS);
        List<WebElement> visibleMessages = getDriver().findElements(By.cssSelector("div#protonMailMessage[style]"));
        Assert.assertEquals(visibleMessages.size(), 1, "Failed to decrypt message");
        return this;
    }
    public String getSubject(){
        waitForElementToAppearByCss("h1#pmTitle",MailPageWithMenus.PAGE_LOAD_TIMEOUT_SECONDS);
        return getByCss(subjectTextCss).getText();
    }
    public String getMessage(){
        waitForElementToAppearByCss("#protonMailMessage",MailPageWithMenus.PAGE_LOAD_TIMEOUT_SECONDS);
        return getByCss(messageTextCss).getText();
    }
    public DecryptedMessageMailPage verifySubject(@Default(string = DefaultTestData.SUBJECT) String subject){
        String actual = getSubject();
        Assert.assertEquals(actual, subject);
        return this;
    }
    public DecryptedMessageMailPage verifyMessage(@Default(string = DefaultTestData.MESSAGE) String message){
        String actual = getMessage();
        Assert.assertEquals(actual, message);
        return this;
    }
}
