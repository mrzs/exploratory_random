package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.common.DefaultTestData;
import explr.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class MainLoginPage extends AbstractPage{
    static private final String URL = "https://protonmail.ch/login";
//    static private final String TITLE = "ProtonMail";
    private String loginInputCSS = "input#username";
    private String passwordInputCSS = "input#Password";
    private String submitButtonCSS = "#login_btn";

    public MainLoginPage(WebDriver driver){
        this(driver, true);
    }
    public MainLoginPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  MainLoginPage getInstance(WebDriver driver) {
        return new MainLoginPage(driver,true);
    }
    public MainLoginPage navigate(){
        getDriver().get(URL);
        return this;
    }
    public MailboxLoginPage login(@Default(string = DefaultTestData.LOGIN ) String login, @Default(string = DefaultTestData.PASSWORD) String password){
        getByCss(loginInputCSS).sendKeys(login);
        getByCss(passwordInputCSS).sendKeys(password);
        getByCss(submitButtonCSS).click();
        return new MailboxLoginPage(getDriver());
    }
}
