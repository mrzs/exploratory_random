package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import org.openqa.selenium.WebDriver;

import static explr.core.common.DefaultTestData.*;

public class InboxMailPage extends MailPageWithMenus<InboxMailPage>{
    static private final String URL = "https://protonmail.ch/inbox";
    static private final String TITLE = "ProtonMail.*";
    public InboxMailPage(WebDriver driver){
        this(driver, true);
    }
    public InboxMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  InboxMailPage getInstance(WebDriver driver) {
        return new InboxMailPage(driver,true);
    }
    public InboxMailPage navigate(){
        getDriver().get(URL);
        return this;
    }

    @Override
    @Depends(clas = {InboxMailPage.class}, method = {"sendNewMessageToMe"})
    public InboxMailPage setMessagesStarred(@Default(bool = IS_STARED) boolean starred, @Default(string = LOGIN) String sender, @Default(string = SUBJECT) String subject) {
        return super.setMessagesStarred(starred, sender, subject);
    }
    @Override
    @Depends(clas = {InboxMailPage.class}, method = {"sendNewMessageToMe"})
    public InboxMailPage deleteMails(@Default(string = SUBJECT) String subject) {
        return super.deleteMails(subject);
    }

    @IgnoreInRandomTesting
    //Helper method that resolves dependencies for this.setMessagesStarred and this.deleteMails (@Default values are not suitable)
    public InboxMailPage sendNewMessageToMe(){
        ComposeMailPage composeMailPage = ComposeMailPage.getInstance(getDriver());
        composeMailPage.setTo(THIS_EMAIL);
        composeMailPage.setSubject(SUBJECT);
        return composeMailPage.send();
    }

    @IgnoreInRandomTesting
    public InboxMailPage getThis(){
        return this;
    }
}
