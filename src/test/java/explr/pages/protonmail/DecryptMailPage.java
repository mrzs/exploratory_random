package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

@IgnoreInRandomTesting
public class DecryptMailPage extends AbstractPage{
    static private final String URL = "https://protonmail.ch/decrypt"; //useless
//    static private final String TITLE = "ProtonMail";
    private String passwordInputCss = "input#frontEndPass";
    private String decryptBtnCss = "button#decryptMsg.pure-button";
    public DecryptMailPage(WebDriver driver){
        this(driver, false);
    }
    public DecryptMailPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    public String getPageUrl(){
        return URL;
    }
    public String getTitle(){
        return TITLE;
    }
    public static  DecryptMailPage getInstance(WebDriver driver) {
        return new DecryptMailPage(driver,true);
    }
    public DecryptMailPage navigate() {
        getDriver().get(URL);
        waitForElementToDisappearByCss("div#loadingArea", PAGE_LOAD_TIMEOUT_SECONDS);
        return this;
    }

    public DecryptedMessageMailPage decrypt(@Default String password){
        getByCss(passwordInputCss).sendKeys(password);
        getByCss(decryptBtnCss).click();
        return new DecryptedMessageMailPage(getDriver(),false);
    }
}