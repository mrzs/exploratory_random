package explr.pages.protonmail;

import explr.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public abstract class AbstractLoggedInPage extends AbstractPage{
    protected static String composeBtnCss = "#compose_button > a";
    protected static String inboxBtnCss = "div#sidebar li > a[href=\"/inbox/d\"]";
    protected static String draftsBtnCss = "div#sidebar li > a[href=\"/draft/d\"]";
    protected static String contactsBtnCss = "div#sidebar li > a[href=\"/contacts\"]";
    protected static String trashBtnCss = "div#sidebar li > a[href=\"/trash/d\"]";
    protected static String starredBtnCss = "div#sidebar li > a[href=\"/starred/d\"]";
    protected static String loadingAreaCss = "div#loadingArea";

    protected AbstractLoggedInPage(WebDriver driver){
        this(driver, true);
    }
    protected AbstractLoggedInPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }

    public abstract <T extends AbstractLoggedInPage>T getThis();

    public InboxMailPage inbox(){
        waitForLoadingArea();
        getByCss(inboxBtnCss).click();
        waitForElementToAppearByCss("body#page-inbox", PAGE_LOAD_TIMEOUT_SECONDS);
        return new InboxMailPage(getDriver(),false);
    }
    public DraftsMailPage drafts(){
        getByCss(draftsBtnCss).click();
        waitForElementToAppearByCss("body#page-draft", PAGE_LOAD_TIMEOUT_SECONDS);
        return new DraftsMailPage(getDriver(),false);
    }
    public ComposeMailPage compose(){
        getByCss(composeBtnCss).click();
        waitForElementToAppearByCss("#MessageTitle", PAGE_LOAD_TIMEOUT_SECONDS);
        return new ComposeMailPage(getDriver(),false);
    }
    public ContactsPage contacts(){
        getByCss(contactsBtnCss).click();
        waitForElementToAppearByCss("table#contactsTable", PAGE_LOAD_TIMEOUT_SECONDS);
        return new ContactsPage(getDriver(),false);
    }
    public TrashMailPage trash(){
        getByCss(trashBtnCss).click();
        waitForElementToAppearByCss("body#page-trash", PAGE_LOAD_TIMEOUT_SECONDS);
        return new TrashMailPage(getDriver(),false);
    }
    public StarredMailPage starred(){
        getByCss(starredBtnCss).click();
        waitForElementToAppearByCss("body#page-starred", PAGE_LOAD_TIMEOUT_SECONDS);
        return new StarredMailPage(getDriver(),false);
    }
    protected void waitForLoadingArea(){
        waitForLoadingArea(3);
    }
    protected void waitForLoadingArea(int timeout){
        waitForElementToDisappearByCss(loadingAreaCss, timeout);
    }
}
