package explr.pages.protonmail;

import com.google.common.base.Function;
import explr.core.annotations.Default;
import explr.pages.protonmail.parts.MailListHeaderMenu;
import explr.pages.protonmail.parts.MailRow;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

import static explr.core.common.DefaultTestData.SENDER;
import static explr.core.common.DefaultTestData.SUBJECT;

public abstract class MailPageWithMenus<T extends MailPageWithMenus> extends AbstractLoggedInPage {
    private static String mailTableCss = "table#messages > tbody > tr";
    private static String selectBySubjectCss = "table#messages > tbody > tr[data-subject=\"%s\"]";
    private static String selectChcBoxCss = "input[type=\"checkbox\"]";
    //    private String epmtyDivCss = "div#listing > div#nothin";
    protected MailListHeaderMenu headerMenu;

    protected MailPageWithMenus(WebDriver driver){
        this(driver, true);
    }
    protected MailPageWithMenus(WebDriver driver, boolean navigate){
        super(driver, navigate);
        headerMenu = new MailListHeaderMenu(driver);
    }

    public abstract T getThis();

    public T verifyMailExists(@Default(string = SENDER) String sender,@Default(string = SUBJECT) String subject){
        waitForLoadingArea();
        MailRow mail = findContact(sender,subject);
        Assert.assertTrue(mail != null);
        return getThis();
    }
    public T verifyMailNotExists(@Default(string = SENDER) String sender,@Default(string = SUBJECT) String subject){
        waitForLoadingArea();
        MailRow mail = findContact(sender,subject);
        Assert.assertTrue(mail == null);
        return getThis();
    }
    public T verifyMessageStarred(@Default(string = SENDER) String sender, @Default(string = SUBJECT) String subject){
        waitForLoadingArea();
        MailRow mail = findContact(sender,subject);
        Assert.assertTrue(mail!=null && mail.isStarred());
        return getThis();
    }
    public T verifyMessageNotStarred(@Default(string = SENDER) String sender, @Default(string = SUBJECT) String subject){
        waitForLoadingArea();
        MailRow mail = findContact(sender,subject);
        Assert.assertTrue(mail!=null && !mail.isStarred());
        return getThis();
    }
    public T selectBySubject(@Default(string = SUBJECT) String subject){
        waitForLoadingArea();
        subject = String.format(selectBySubjectCss,subject);
        List<WebElement> mails = getDriver().findElements(By.cssSelector(subject));
        for(WebElement mail : mails){
            WebElement chkBox = mail.findElement(By.cssSelector(selectChcBoxCss));
            if(!chkBox.isSelected()){
                chkBox.click();
            }
        }
        return getThis();
    }
    //made protected because @Depends annotation value depends on subclass
    protected T setMessagesStarred(final boolean starred, String sender,String subject){
        waitForLoadingArea();
        final MailRow mail = findContact(sender,subject);
        String notFoundMessage = String.format("An email with sender:'%s' and subject:'%s' was not found.", sender, subject);
        Assert.assertTrue(mail != null, notFoundMessage);
        mail.setStared(starred);
        WebDriverWait wait = new WebDriverWait(getDriver(),2);
        wait.until(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver input) {
                return mail.isStarred() == starred;
            }
        });
        return getThis();
    }
    //made protected because @Depends annotation value depends on subclass
    protected T deleteMails(String subject){
        waitForLoadingArea();
        selectBySubject(subject);
        headerMenu.delete();
        return getThis();
    }
    public List<WebElement> findBySubject(@Default(string = SUBJECT) String subject){
        subject = String.format(selectBySubjectCss,subject);
        List<WebElement> elements = getDriver().findElements(By.cssSelector(subject));
        return elements;
    }
    private MailRow findContact(String sender, String subject){
        List<WebElement> rows = getDriver().findElements(By.cssSelector(mailTableCss));
        for(WebElement rowElement : rows){
            MailRow row = new MailRow(rowElement);
            if(sender.equals(row.getSender()) && subject.equals(row.getSubject())){
                return row;
            }
        }
        return null;
    }
//    public boolean isEmpty(){
//        return getDriver().findElements(By.cssSelector(epmtyDivCss)).size() > 0;
//    }
}
