package explr.pages.protonmail;

import explr.core.annotations.Default;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.common.DefaultTestData;
import explr.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class MailboxLoginPage extends AbstractPage {
    static private final String URL = "https://protonmail.ch/locked";
//    static private final String TITLE = "ProtonMail";
    private String mailboxPasswordInputCSS = "#MailboxPassword";
    private String unlockButtonCSS = "#enck";
    public MailboxLoginPage(WebDriver driver){
        this(driver, false);
    }
    public MailboxLoginPage(WebDriver driver, boolean navigate){
        super(driver, navigate);
    }
    @IgnoreInRandomTesting
    public String getPageUrl(){
        return URL;
    }
    @IgnoreInRandomTesting
    public String getTitle(){
        return TITLE;
    }
    public static  MailboxLoginPage getInstance(WebDriver driver) {
        return new MailboxLoginPage(driver,true);
    }
    public MailboxLoginPage navigate(){
        getDriver().get(URL);
        return this;
    }
    public InboxMailPage unlockMailbox(@Default(string = DefaultTestData.MAILBOX_PASSWORD) String password){
        waitForElementToAppearByCss(mailboxPasswordInputCSS, PAGE_LOAD_TIMEOUT_SECONDS);
        getByCss(mailboxPasswordInputCSS).sendKeys(password);
        getByCss(unlockButtonCSS).click();
        waitForElementToDisappearByCss("div#loadingArea", PAGE_LOAD_TIMEOUT_SECONDS);
        return new InboxMailPage(getDriver(),true);
    }
}
