package explr.core.executors;

import explr.core.annotations.Default;
import explr.core.common.ClassMethodPair;
import explr.core.common.TestCase;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SaveToFileExecutor implements TesCaseExecutor {
    private static final String GENERATED_FOLDER_PATH = "./src/test/java/explr/tests/generated/";

    @Override
    public void execute(TestCase testCase) {
        String indent = "        ";
        String className = "generatedTest_"+(testCase.getSeed() < 0 ? "neg"+(-testCase.getSeed()) : testCase.getSeed());
        List<ClassMethodPair> mthds = testCase.getMethodsAndVerifications();
        Set<String> declaredVariables = new TreeSet<String>();
        StringBuilder codeBuilder = new StringBuilder();
        String initialClass = mthds.get(0).getClaz().getSimpleName();
        codeBuilder.append(indent + initialClass+" "+initialClass.toLowerCase()+" = new "+ initialClass + "(driver, true);\n");
        declaredVariables.add(initialClass);
        for(ClassMethodPair pair : mthds){
            Class decClass = pair.getClaz();
            Method mtd = pair.getMethod();
            if(mtd!=null) {
                String returnType = mtd.getReturnType().getSimpleName();
                String variableAssignment = "";
                if(!returnType.equals("void")) {
                    variableAssignment = returnType + " " + returnType.toLowerCase() + " = ";
                }
                String mtdCall;
                if(Modifier.isStatic(mtd.getModifiers())){
                    mtdCall = decClass.getSimpleName()+"."+ mtd.getName() + methodParamsValuesToString(mtd)+";\n";
                }else {
                    mtdCall = decClass.getSimpleName().toLowerCase() +"."+ mtd.getName() + methodParamsValuesToString(mtd)+";\n";
                }
                if(!declaredVariables.contains(returnType)) {
                    codeBuilder.append(indent + variableAssignment + mtdCall);
                    declaredVariables.add(returnType);
                }else{
                    codeBuilder.append(indent + mtdCall);
                }
            }else{
                codeBuilder.append("//Corresponding method Not Found\n");
            }
        }
        String classCode = String.format(DA_TEST,className,codeBuilder.toString());
        write(className,classCode);
    }

    private void write(String className, String code){
        checkFolderExists();
        String testCasePath = GENERATED_FOLDER_PATH + className + ".java";
        FileWriter fileWriter = null;
        try {
            fileWriter= new FileWriter(new File(testCasePath));
            fileWriter.write(code);
        }catch (IOException e){
            //TODO logging
        }finally {
            if(fileWriter!=null){
                try {fileWriter.close();}
                catch (IOException e){}
            }
        }
    }

    private void checkFolderExists(){
        File folder = new File(GENERATED_FOLDER_PATH);
        if(!folder.exists()){
            folder.mkdirs();
        }
    }

    private String methodParamsValuesToString(Method mtd){
        StringBuilder sb = new StringBuilder();
        if(mtd!=null) {
            sb.append("(");
            Annotation[][] parameterAnnotations = mtd.getParameterAnnotations();
            Class[] params = mtd.getParameterTypes();
            for (int i = 0; i < params.length; i++) {
                Annotation[] annotations = parameterAnnotations[i];
                Class param = params[i];
                Default defValueAnnotation = null;
                for (Annotation ann : annotations){
                    if(ann instanceof Default){
                        defValueAnnotation = (Default)ann;
                        break;
                    }
                }
                if (defValueAnnotation != null) {
                    if(param.equals(String.class)){
                        sb.append("\""+defValueAnnotation.string()+"\"");
                    }else if(param.equals(Integer.class) || param.equals(int.class)){
                        sb.append(defValueAnnotation.integer());
                    } else if (param.equals(Boolean.class) || param.equals(boolean.class)){
                        sb.append(defValueAnnotation.bool());
                    }else if(param.equals(Long.class) || param.equals(long.class)){
                        sb.append(defValueAnnotation.longh());
                    } else if(param.equals(WebDriver.class)){
                        sb.append("driver"); // for methods that take WebDriver as a parameter and its variable name is "driver"
                    }else{
                        sb.append(param.getSimpleName()); // for all unsupported types =(
                    }
                }else{
                    if(param.equals(WebDriver.class)){
                        sb.append("driver"); // for methods that take WebDriver as a parameter and its variable name is "driver"
                    }else {
                        sb.append(param.getSimpleName()); // same for unsupported if annotation is missing
                    }
                }
                if (i != params.length - 1) {
                    sb.append(", ");
                }
            }
            sb.append(")");
        }else{
            sb.append("NULL_METHOD"); // throw smthing?
        }
        return sb.toString();
    }

    static private final String DA_TEST =
            "package explr.tests.generated;\n\n" +

            "import explr.pages.protonmail.*;\n" +
            "import org.openqa.selenium.WebDriver;\n" +
            "import java.util.List;\n" +
            "import explr.core.common.DefaultTestData;\n" +
            "import explr.pages.AbstractPage;\n" +
            "import explr.tests.AbstractTestCase;\n" +
            "import org.testng.annotations.*;\n" +
            "import explr.pages.TenMinutesMailPage;\n" +
            "import org.junit.BeforeClass;\n\n" +

            "public class %s extends AbstractTestCase{\n" +
            "    private String login = DefaultTestData.LOGIN;\n" +
            "    private String password = DefaultTestData.PASSWORD;\n" +
            "    private String mailboxPassword = DefaultTestData.MAILBOX_PASSWORD;\n\n" +
//            "    @BeforeClass\n" +
//            "    @Test(dataProvider = \"WebDriverProvider\")\n" +
//            "    public void init(WebDriver driver){\n" +
//            "        login(driver);\n" +
//            "    }\n\n"+
            "    @Test(dataProvider = \"WebDriverProvider\")\n" +
            "    public void test(WebDriver driver){\n" +
            "        login(driver);\n" +
                    "//****<Generated>****\n"+
            "%s" +
                    "//****</Generated>****\n"+
            "    }\n\n" +

            "    private InboxMailPage login(WebDriver driver){\n" +
            "        return new MainLoginPage(driver)\n" +
            "                .login(login, password)\n" +
            "                .unlockMailbox(mailboxPassword);\n" +
            "    }\n" +
            "    public %1$2s(){\n" +
            "        restartBrowserForEachTestCase = true;\n" +
            "    }\n" +

//            "  @AfterMethod\n" +
//            "    public void qtBrw(){\n" +
//            "         tearDown();\n" +
//                "}"+

            "}";
}
