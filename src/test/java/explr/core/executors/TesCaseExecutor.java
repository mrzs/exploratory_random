package explr.core.executors;

import explr.core.common.TestCase;

public interface TesCaseExecutor {
    void execute(TestCase testCase);
}
