package explr.core;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.util.ArrayList;
@Deprecated
public class Oracle implements IInvokedMethodListener{
    private final ArrayList<String> ignoredMessages = new ArrayList<String>();

    public Oracle(){
        ignoredMessages.add("Failed to decrypt message expected [1] but found [0]");
        ignoredMessages.add("An email with sender:'me@myself.com' and subject:'Seen that?' was not found. expected [true] but found [false]");
        ignoredMessages.add("Delete button is not enabled.");
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
//        if(testResult.getStatus() == ITestResult.FAILURE){
//            Throwable error = testResult.getThrowable();
//            String errorMessage = error.getMessage();
//            if(ignoredMessages.contains(errorMessage)){
//                testResult.setStatus(ITestResult.SUCCESS_PERCENTAGE_FAILURE);
//                System.out.println("Filtered by Oracle:");
//                error.printStackTrace();
//            }
//        }
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
    }
}
