package explr.core.common;

public final class DefaultTestData {
    public static final String LOGIN = "autotest";
    public static final String PASSWORD = "mySuperPa33";
    public static final String MAILBOX_PASSWORD = "mymailboxPa33";
    public static final boolean IS_STARED = true;
    public static final boolean IS_NOT_STARED = false;
    public static final String SENDER = "me@myself.com";
    public static final String THIS_EMAIL = "autotest@protonmail.ch";
    public static final String SUBJECT = "Seen that?";
    public static final String MESSAGE = "Look at my mail! My mail is amazing!";

    public static final String CONTACT_NAME = "Rob Johnson";
    public static final String CONTACT_EMAIL = "robin@john.com";
    public static final String CONTACT_EDITED_NAME = "Rob Junior Johnson";
    public static final String CONTACT_EDITED_EMAIL = "robin-juniour@john.com";

    public static final String MAIL_PASSWORD = "myMessagePass";
    public static final String MAIL_PASS_HINT = "Hey!<p> I am a hint!";
}