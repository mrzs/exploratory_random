package explr.core.common;

import java.lang.reflect.Method;
import java.util.Set;

public class TestClassAssets{
//    private final Set<Constructor> constructors;
    private final Set<Method> steps;
    private final Set<Method> verifications;

    public TestClassAssets(/*Set<Constructor> constructors,*/ Set<Method> steps, Set<Method> verifications){
//        this.constructors = constructors;
        this.steps = steps;
        this.verifications = verifications;
    }
//    public Set<Constructor> getConstructors() {
//        return constructors;
//    }
    public Set<Method> getSteps() {
        return steps;
    }
    public Set<Method> getVerifications() {
        return verifications;
    }
}
