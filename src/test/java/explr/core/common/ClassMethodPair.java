package explr.core.common;

import java.lang.reflect.Method;

public class ClassMethodPair {
    private Class claz;
    private Method method;

    public ClassMethodPair(Class claz, Method method) {
        this.claz = claz;
        this.method = method;
    }

    public Class getClaz() {
        return claz;
    }
    public Method getMethod() {
        return method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassMethodPair that = (ClassMethodPair) o;

        if (!claz.equals(that.claz)) return false;
        if (method != null ? !method.equals(that.method) : that.method != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = claz.hashCode();
        result = 31 * result + (method != null ? method.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "{"+ claz.getSimpleName() +" :: " + method.getName() +'}';
    }
}
