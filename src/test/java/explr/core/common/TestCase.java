package explr.core.common;

import explr.core.executors.SaveToFileExecutor;
import explr.core.executors.TesCaseExecutor;
import explr.utils.Utils;

import java.lang.reflect.Method;
import java.util.List;

public class TestCase {
    private long seed;
    private List<ClassMethodPair> methodsAndVerifications;
    private TesCaseExecutor executor;

    public TestCase(long seed, List<ClassMethodPair> methodsAndVerifications) {
        this(seed, methodsAndVerifications, new SaveToFileExecutor());
    }
    public TestCase(long seed, List<ClassMethodPair> methodsAndVerifications, TesCaseExecutor executor) {
        this.seed = seed;
        this.executor = executor;
        this.methodsAndVerifications = methodsAndVerifications;
    }

    public void execute(){
        executor.execute(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(ClassMethodPair pair : methodsAndVerifications){
            Method mtd = pair.getMethod();
            sb.append("\n"+pair.getClaz().getSimpleName()+"."+(mtd==null ? null : mtd.getName()));
            sb.append(Utils.methodParamsToString(mtd));
            sb.append(" -> " + mtd.getReturnType().getSimpleName());
        }
        return sb.toString();
    }

    public long getSeed(){
        return seed;
    }
    public List<ClassMethodPair> getMethodsAndVerifications(){
        return methodsAndVerifications;
    }
}
