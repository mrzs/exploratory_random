package explr.core;

import com.google.common.base.Predicate;
import explr.core.annotations.Depends;
import explr.core.annotations.IgnoreInRandomTesting;
import explr.core.annotations.OnPageChange;
import explr.core.common.ClassMethodPair;
import explr.core.common.TestCase;
import explr.core.common.TestClassAssets;
import org.openqa.selenium.WebDriver;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import static org.reflections.ReflectionUtils.*;

public class TestCaseGenerator {
    /** Name of the package in to look for page object classes */
    private static final String PACKAGE_TO_SEARCH_FOR_CLASSES = "explr";
    /** Class names that contain this string will be treated as a classes with business methods (aka steps) */
    private static final String PAGE_OBJECT_ID_STRING = "Page";
    /** Method name that checks if page is OK. Should be present in all PageObjects and to have a return type void */
    private static final String SANITY_CHECK_METHOD_NAME = "sanityCheck";
    /** Method names that match this pattern will be treated as test steps. Default says "any that does not contain string 'verify'" */
    private static final String STEP_NAME_PATTERN = String.format("^((?!(verify|%s)).)*$",SANITY_CHECK_METHOD_NAME);
    /** Method names that match this pattern will be treated as verification steps. Default says "any that contain string 'verify'" */
    private static final String VERIFICATION_NAME_PATTERN = String.format(".*verify.*|.*%s.*",SANITY_CHECK_METHOD_NAME);
    /** A percent probability of randomly "jumping" to another page in the flow of generated test case */
    private static final float CHANCE_TO_SWITCH_PAGE = 0.1f; //10%
    /** A maximum number of dependencies that method can have */
    private static final int MAX_DEPENDENCIES = 1000;
    /** A name of a method that is used to "construct" a PageObject */
    private static final String GET_INSTANCE_METHOD_NAME = "getInstance";

    private int verificationStrategy;
    public static final int USE_RANDOM_VERIFICATIONS = -300;
    public static final int USE_PAGE_SANITY_VERIFICATIONS = 700;
//    public static final int USE_PAGE_SPECIFIC__VERIFICATIONS = ;
//    public static final int USE_PAGE_SANITY_AND_RANDOM__VERIFICATIONS = ;
//    public static final int USE_ALL__VERIFICATIONS = ;

    private Map<Class,TestClassAssets> classesAndAssets;
    private long seed;
    /**
     * @param numberOfSteps - a number of steps to be generated.
     * @return ArrayList<Method> with size() that is stepsNmbr*2. It is assumed that each test step is followed by a verification step.
     */
    public TestCase generateTestCase(int numberOfSteps) {
        return generateTestCase(numberOfSteps,USE_PAGE_SANITY_VERIFICATIONS);
    }
    /**
     * @param numberOfSteps - a number of steps to be generated.
     * @param verificationStrategy - a constant that represents a strategy for verify step generation
     * @return ArrayList<Method> with size() that is stepsNmbr*2. It is assumed that each test step is followed by a verification step.
     */
    public TestCase generateTestCase(int numberOfSteps, int verificationStrategy) {
        this.verificationStrategy = verificationStrategy;
        seed = seedUniquifier() ^ System.nanoTime();
        Random rand = new Random(seed);
        classesAndAssets = getClasses();
        ArrayList<ClassMethodPair> testcase = new ArrayList<ClassMethodPair>(numberOfSteps*2); // step + verification steps
        Class[] classes = classesAndAssets.keySet().toArray(new Class[0]);
        Class aClass = classes[rand.nextInt(classes.length)];
        for(int i=0; i<numberOfSteps; i++){
            Method step = addStep(aClass, testcase, rand);
            aClass = getAppropriateClass(aClass,step);
            addVerification(aClass,testcase, rand);
        }
        return new TestCase(seed, validateFlow(testcase));
    }

    private Method addStep(Class aClass, ArrayList<ClassMethodPair> testcase, Random rand) {
        TestClassAssets testClassAssets = classesAndAssets.get(aClass);
        Method[] steps = testClassAssets.getSteps().toArray(new Method[0]);
        Method step = getRandomStepFromArray(steps,rand);
        checkDependencies(step, testcase); // Oh...
        testcase.add(new ClassMethodPair(aClass,step));
        return step;
    }
    private void checkDependencies(Method step, ArrayList<ClassMethodPair> testcase) {
        LinkedList<ClassMethodPair> dependencies = new LinkedList<ClassMethodPair>();
        dependencies = getDependencies(step);
//        int i = 0;
//        while(dependency!=null && i<MAX_DEPENDENCIES){ // form method dependency list
//            dependencies.add(dependency);
//            dependency = getDependency(dependency.getMethod());
//            i++;
//        }
        addAndResolveDependencies(testcase, dependencies);
    }

    private void addAndResolveDependencies(ArrayList<ClassMethodPair> testcase, LinkedList<ClassMethodPair> dependencies) {
        for(int j = dependencies.size() - 1; j>=0; j--) { // insert dependencies into testcase
            ClassMethodPair resolveDependencyToAdd = resolveAbstractsInDependencies(j, dependencies);
            if (resolveDependencyToAdd != null /* && !testcase.contains(resolveDependencyToAdd) TODO possible bug? track this on class level? */) {
                testcase.add(resolveDependencyToAdd);
            }
        }
    }

    private ClassMethodPair resolveAbstractsInDependencies(int dependencyIndex, LinkedList<ClassMethodPair> dependencies) { //UseLess?
        ClassMethodPair dependencyToAdd = dependencies.get(dependencyIndex);
        if (dependencyToAdd==null) return null;
        Class dependencyToAddClass = dependencyToAdd.getClaz();
        if(Modifier.isAbstract(dependencyToAddClass.getModifiers())){
            for(int i = dependencyIndex+1; i < dependencies.size() ; i++ ){
                Class previousClass = dependencies.get(i).getClaz();
                if(dependencyToAddClass.equals(previousClass.getSuperclass())){
                    return new ClassMethodPair(previousClass,dependencyToAdd.getMethod());  //omg
                }
            }
        }
        return dependencyToAdd;
    }

    //    private boolean checkIfTestCaseContainsClass(Class clas,ArrayList<ClassMethodPair> testcase){
//        for(ClassMethodPair pair : testcase){
//            if(pair.getClass().equals(clas)){
//                return true;
//            }
//        }
//        return false;
//    }
    private LinkedList<ClassMethodPair> getDependencies(Method step) {
        LinkedList<ClassMethodPair> dependencies = new LinkedList<ClassMethodPair>();
        Depends dependsOn = step.getAnnotation(Depends.class);
        if(dependsOn != null){
            Class[] classes = dependsOn.clas();
            String[] methodNames = dependsOn.method();
            if(classes.length != methodNames.length){
                throw new IllegalStateException("Step "+step+"has unequal number of classes and methods in its @Depends annotation.");
            }
            for(int i=0;i<classes.length;i++){
                Class clz = classes[i];
                String methodName = methodNames[i];
                Method methodDependency = getMethodByName(clz, methodName);
                dependencies.add(new ClassMethodPair(clz,methodDependency));
            }
        }
        return dependencies;
    }

    private Method getMethodByName(Class clz, String methodName) {
        Method methodDependency = null;
        for(Method method : clz.getMethods()){
            if(method.getName().equals(methodName)){
                if(areModifiersAcceptable(method)){
                    methodDependency = method;
                    break;
                }
            }
        }
        return methodDependency;
    }

    private Method getRandomStepFromArray(Method[] steps, Random rand){
        Method step = steps.length==0 ? null : steps[rand.nextInt(steps.length)];
        if(step.getName().contains("jumpTo")){             // if method "jumps"
            if(rand.nextFloat() <= CHANCE_TO_SWITCH_PAGE){ // we roll dice to check if the chance will hit
                return step;                               // if success, than we jump to another page
            }else{
                return getRandomStepFromArray(steps,rand); // if failed, we try another method
            }
        }
        return step;
    }
    private Class getAppropriateClass(Class aClass,Method step) {
        if (step != null) {
            Class stepReturnType = step.getReturnType();
            if(!stepReturnType.equals(Void.TYPE) && classesAndAssets.containsKey(stepReturnType)){
                aClass = stepReturnType;
            }
        }
        return aClass;
    }
    private Method addVerification(Class aClass, ArrayList<ClassMethodPair> testcase, Random rand) {
        Method[] verifications = classesAndAssets.get(aClass).getVerifications().toArray(new Method[0]);
        Method verification;
        switch (verificationStrategy) {
            case USE_RANDOM_VERIFICATIONS:
                verification = verifications.length == 0 ? null : verifications[rand.nextInt(verifications.length)];
                break;
            case USE_PAGE_SANITY_VERIFICATIONS:
                try {
                    verification = aClass.getMethod(SANITY_CHECK_METHOD_NAME);
                    break;
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            default:
                verification = null;
        }
        testcase.add(new ClassMethodPair(aClass,verification));
        return verification;
    }

    private Map<Class,TestClassAssets> getClasses(){
        Map<Class,TestClassAssets> classesAndAssets = new HashMap<Class,TestClassAssets>();

        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(PACKAGE_TO_SEARCH_FOR_CLASSES)))
        );
        Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
        Predicate  filterMethods = new Predicate<Method>(){
            @Override
            public boolean apply(@Nullable Method method){
                boolean ignore = method.getAnnotation(IgnoreInRandomTesting.class) != null;
                return !ignore && areModifiersAcceptable(method);
            }
        };
        for(Class cls : classes){
            Annotation ignoreInRandomTesting = cls.getAnnotation(IgnoreInRandomTesting.class);
            if(ignoreInRandomTesting!=null) continue;
            if(cls.getSimpleName().contains(PAGE_OBJECT_ID_STRING)) {
                if(!Modifier.isAbstract(cls.getModifiers())) {
//                    Set<Constructor> constructors = getConstructors(cls, withModifier(Modifier.PUBLIC));
                    Set<Method> steps = getAllMethods(cls, withModifier(Modifier.PUBLIC), withPattern(STEP_NAME_PATTERN), filterMethods);
                    Set<Method> verifications = getAllMethods(cls, withModifier(Modifier.PUBLIC), withPattern(VERIFICATION_NAME_PATTERN)/*, withParametersCount(1)*/);
                    classesAndAssets.put(cls, new TestClassAssets(/*constructors, */steps, verifications));
                }
            }
        }
        return classesAndAssets;
    }

    /**
     *  copy-pasted from java.lang.Random
     */
    private static long seedUniquifier() {
        // L'Ecuyer, "Tables of Linear Congruential Generators of
        // Different Sizes and Good Lattice Structure", 1999
        for (;;) {
            long current = seedUniquifier.get();
            long next = current * 181783497276652981L;
            if (seedUniquifier.compareAndSet(current, next))
                return next;
        }
    }
    /** copy-pasted from java.lang.Random */
    private static final AtomicLong seedUniquifier
            = new AtomicLong(8682522807148012L);

    private ArrayList<ClassMethodPair> validateFlow(ArrayList<ClassMethodPair> testcase){
        ArrayList<ClassMethodPair> validatedTestCase = new ArrayList<ClassMethodPair>(testcase.size() + testcase.size()/2); // add some room just in case
//        Class currentPage = testcase.isEmpty() ? null : testcase.get(0).getClaz();
//        Class lastPageObject = void.class;
        Class lastReturnType = void.class;
        for(int i = 0; i < testcase.size(); i++){
            ClassMethodPair step = testcase.get(i);
            Class stepReturnType = step.getMethod().getReturnType();
            stepReturnType = classesAndAssets.containsKey(stepReturnType) ? stepReturnType : void.class;
//            lastPageObject = step ? lastReturnType : stepReturnType;
            lastReturnType = stepReturnType.equals(void.class) ? lastReturnType : stepReturnType;
            boolean hasNext = !(i+1 == testcase.size());
            Class expectedClass = hasNext ? testcase.get(i + 1).getClaz() : lastReturnType;
            boolean isAbstract = Modifier.isAbstract(expectedClass.getModifiers());
            validatedTestCase.add(step); // adding step first
            if (!isAbstract && !expectedClass.equals(void.class) && !lastReturnType.equals(expectedClass)){  // Add getInstance call if needed
                try {
                    Method getInstanceMethod = expectedClass.getDeclaredMethod(GET_INSTANCE_METHOD_NAME,WebDriver.class);
                    addOnPageChangeMethods(step.getClaz(),validatedTestCase);
                    validatedTestCase.add(new ClassMethodPair(expectedClass,getInstanceMethod));
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
//            if(Modifier.isAbstract(step.getClaz().getModifiers())){ // verify there is no method calls on abstract methods
//                validatedTestCase.add(new ClassMethodPair(previous,step.getMethod()));
//            }else{
//                validatedTestCase.add(step);
//            }
//            validatedTestCase.add(step);
        }
        return validatedTestCase;
    }


    private void addOnPageChangeMethods(Class clzz, ArrayList<ClassMethodPair> validatedTestCase) {
        OnPageChange onPageChange = (OnPageChange)clzz.getAnnotation(OnPageChange.class);
        if(onPageChange!=null){
            Class clas = onPageChange.clas();
            Method methodByName = getMethodByName(clas, onPageChange.method());
            validatedTestCase.add(new ClassMethodPair(clas,methodByName));
        }
    }

    private boolean areModifiersAcceptable(Method mtd){
        int modifiers = mtd.getModifiers();
        boolean isAbstract = Modifier.isAbstract(modifiers);
        boolean isStatic = Modifier.isStatic(modifiers);
        boolean isBridge = mtd.isBridge();
        boolean isSynthetic = mtd.isSynthetic();
        boolean isVolatile = Modifier.isVolatile(modifiers);
        return !isAbstract && !isStatic && !isBridge && !isSynthetic && !isVolatile;
    }

    private void saveDebugInfoAsText(Map<Class,TestClassAssets> classesAndAssets, File fileToSave){
//        String testCasePath = "C:\\all.txt";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("seed: "+seed+"\n");
        stringBuilder.append("verificationStrategy: "+verificationStrategy+"\n\n");
        for(Class clz : classesAndAssets.keySet()){
            stringBuilder.append(clz.getSimpleName()+":\n");
            stringBuilder.append("\tSteps:\n");
            for(Method mtd : classesAndAssets.get(clz).getSteps()){
                stringBuilder.append("\t\t"+mtd.getName()+"\n");
            }
            stringBuilder.append("\tVerifications:\n");
            for(Method mtd : classesAndAssets.get(clz).getVerifications()){
                stringBuilder.append("\t\t"+mtd.getName()+"\n");
            }
        }
        FileWriter fileWriter = null;
        try {
            fileWriter= new FileWriter(fileToSave);
            fileWriter.write(stringBuilder.toString());
        }catch (IOException e){
            //TODO logging
        }finally {
            if(fileWriter!=null){
                try {fileWriter.close();}
                catch (IOException e){}
            }
        }
    }
}
