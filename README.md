# README #

### How do I get set up? ###

This is a Java/Maven project. To run tests please use 'mvn test' command. In order to change tests being execcuted please edit ```pom.xml``` file and ```run_generated.xml```,```test.xml``` testng files.

See `explr.tests.TestCaseGeneratorTest` class for test case generation logic.

See also [Hit or Miss: Reusing Selenium Scripts in Random Testing](https://www.infoq.com/articles/selenium-scripts-random)